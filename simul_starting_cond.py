#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 10:35:47 2019

@author: samuel
"""

import mtlheartsim as mtl
import numpy as np

shape = 500, 250

left_half = np.zeros(shape, dtype=int)
left_half[:250, :] = 1

bottom_half = np.zeros(shape, dtype=int)
bottom_half[:, :125] = 1
rest= np.ones(shape, dtype=int)*-80
sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
#sim.property({'G_si': 0.07, 'G_K': 0.423})

sim.initial_condition(Vm=np.asarray(list(-80*left_half.ravel())))
sim.stimulus(time=410, nodes=np.where(bottom_half.ravel())[0])
sim.output_state(filename='dumpfinal_CRN', final = True, enable = True)
sim.coupling(sigma=(0.5, 0.5))
sim.output_dir('/home/saliani/repos/spiralFA/Initial_sim_CRN5')
sim.time_integration(800)
sim.output_maps(spacing=5)
sim.output_activ()
sim.output_var()
sim.run()