import numpy as np
import mtlheartsim as mtl
import matplotlib.pyplot as plt
from setup import setup_data_single
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
from setup import setup_data2
import copy
import glob
#
thresholds = [-20, -40, -60]
models = ['LR1', 'CRN2', 'RCN']
#
#filename = '/mnt/data/samuel/S1/activ_40_RCN'
#actmap = mtl.activ_map(filename)
#actmap.generate((500, 250))
#
#info = mtl.read(filename, only_info=True)
#dims = info['dims']
#
#delays = []
#for i in range(200):
#    ta = actmap.depol_map.reshape(dims)
#    dta = np.abs(ta[1:, :] - ta[:-1, :]).ravel()
#    delays.append(dta)
#    dta = np.abs(ta[:, 1:] - ta[:, :-1]).ravel()
#    delays.append(dta)
#    actmap.advance(10)
#
#delays = np.concatenate(tuple(delays))/1000
#bins, edges = np.histogram(delays, bins=np.arange(100)/4, density = True)
#left,right = edges[:-1],edges[1:]
#X = np.array([left,right]).T.flatten()
#Y = np.array([bins,bins]).T.flatten()
#plt.figure()
#plt.plot(X,Y)
#plt.xlim([0,15])
#plt.yscale('log')
#plt.show()

#plt.figure()
#plt.hist(delays, bins=np.arange(100)/4, density=True)
#plt.yscale('log')
#plt.show()
def cycle_calc(filename, tmax, dt):
    #dims = info['dims']
    cycles = []
    actmap1 = mtl.activ_map(filename)
    actmap2 = mtl.activ_map(filename)
    actmap1.generate(0) # generate an activation map from data up to t= 0 ms
    actmap2.generate(0)
    last_map = actmap2.depol_map # depolarization map
    actmap1.advance(dt) #update map by advancing 5 ms 
    current = actmap1.depol_map
    
    for i in range(int(tmax/dt)-1):
        cycle = (np.asarray(current[np.where((current!=-1) & (last_map!=-1))[0]] - last_map[np.where((current!=-1) & (last_map!=-1))[0]]))
        if len(np.where(cycle!= 0)[0]) != 0:
            #print(type(np.where(cycle!= 0)[0]))
            cycle = cycle[np.where(cycle!= 0)[0]]
            cycles.append(cycle)
        actmap1.advance(dt)
        actmap2.advance(dt)
        last_map = actmap2.depol_map
        current = actmap1.depol_map
#        if i == 50:
#            return [current, last_map, current - last_map, pos]
#        ta[pos] = copy.deepcopy(current[pos])
#        
#        activation_pos = np.where(np.asarray(ta) != -1)[0]
#        candidates = last_activs[activation_pos]
#        
#        to_set = activation_pos[np.where(candidates == -1)[0]]
#        if len(to_set != 0):
#            last_activs[to_set] = copy.deepcopy(ta[to_set])
#            
#        to_cycle = activation_pos[np.where(candidates != -1)[0]]
#        if len(to_cycle) != 0:
#            cycles_v = np.asarray(ta[to_cycle]) - np.asarray(last_activs[to_cycle])
#            print('check')
#            print(len(candidates))
#            print([np.asarray(ta[to_cycle]), np.asarray(last_activs[to_cycle])])
#            print(cycles_v)
#            cycles.append(cycles_v[np.nonzero(cycles_v)])
#            last_activs[to_cycle] = copy.deepcopy(ta[to_cycle])
    cycles = np.concatenate(tuple(cycles))/1000 
    return cycles

def cycle_maps(filename, tmax, dt, shape, model):
    actmap1 = mtl.activ_map(filename)
    actmap2 = mtl.activ_map(filename)
    actmap1.generate(0)
    actmap2.generate(0)
    last_map = actmap2.depol_map
    actmap1.advance(dt)
    current = actmap1.depol_map
    values = [ [] for i in range(len(current))]
    for i in range(int(tmax/dt)-1):
        cycle = (np.asarray(current - last_map))
        indexes = np.where((cycle != 0) & (last_map!=-1))[0]
        if len(indexes) != 0:
#            print(indexes)
#            values[indexes].append(cycle[indexes])
            for j in range(len(indexes)):
                values[indexes[j]].append(cycle[indexes[j]])
        actmap1.advance(dt)
        actmap2.advance(dt)
    stds = np.zeros(len(current))
    means = np.zeros(len(current))
    for i in range(len(current)):
        stds[i] = np.std(np.asarray(values[i])/1000)
        means[i] = np.mean(np.asarray(values[i])/1000)
    std_array = setup_data_single(stds, shape[0], shape[1])
    mean_array = setup_data_single(means, shape[0], shape[1])
    fig, ax = plt.subplots(2, 1, sharex='col', sharey='row')
    X = np.arange(0, int(shape[0]*0.02), 0.02)
    Y = np.arange(0, int(shape[1]*0.02), 0.02)
    Im1 = ax[0].pcolormesh(X, Y,mean_array, cmap=plt.cm.get_cmap('hot'))
    
    divider = make_axes_locatable(ax[0])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(Im1, cax=cax, orientation='vertical')
    ax[0].axis([0, int(shape[0]*0.02), 0, int(shape[1]*0.02)])
    ax[0].set_aspect('equal')
    ax[0].set_ylabel('Mean')
    ax[0].set_title('Cell cycle length distributions ' + model)

    Im2 = ax[1].pcolormesh(X, Y,std_array, cmap=plt.cm.get_cmap('hot'))
    
    divider2 = make_axes_locatable(ax[1])
    cax2 = divider2.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(Im2, cax=cax2, orientation='vertical')
    ax[1].axis([0, int(shape[0]*0.02), 0, int(shape[1]*0.02)])
    ax[1].set_aspect('equal')
    ax[1].set_ylabel('Standard Deviation')
    
def get_moment_delay(filename, limit): 
    actmap = mtl.activ_map(filename)
    actmap.generate((500, 250))
        
    info = mtl.read(filename, only_info=True)
    dims = info['dims']
        
    delays = []
    high_delays = np.zeros(250)
    for i in range(250):
        ta = actmap.depol_map.reshape(dims)
        dta = np.abs(ta[1:, :] - ta[:-1, :]).ravel()
        if len(np.where(np.asarray(dta/1000)>limit)[0]) > 0:
            print(len(np.where(np.asarray(dta/1000)>limit)[0]))
            high_delays[i] = 1  
        delays.append(dta)
        dta = np.abs(ta[:, 1:] - ta[:, :-1]).ravel()
        if len(np.where(np.asarray(dta/1000)>limit)[0]) > 0:
            print(len(np.where(np.asarray(dta/1000)>limit)[0]))
            high_delays[i] = 1  
        delays.append(dta)
        actmap.advance(10)
    return high_delays

def plot_high_delays(substrate, limit, t,dx, dt, tmax): 
    BASE_DIR = '/mnt/data/samuel/new/PS_simlib/'+substrate
    activ_files = glob.glob(BASE_DIR+'/*/activ')
    simdirs = [f[:-5] for f in activ_files]
    for k in range(len(simdirs)):
        filename = simdirs[k]+'activ'
        actmap = mtl.activ_map(filename)
        actmap.generate(500)
        info = mtl.read(filename, only_info=True)
        dims = info['dims']
        time = 500
        delays = []
        interv = t
        for i in range(200):
            high_pos_x = []
            high_pos_y = []
            ta = actmap.depol_map.reshape(dims)
            ta_padded_a0 = np.insert(ta, 0, values=0, axis=0)
            ta_padded_a1 = np.insert(ta, 0, values=0, axis=1)
            temp_a0 = np.insert(ta[1:,:-1], 499, values=0, axis=0)
            temp_a1 = np.insert(ta[:-1,1:], 249, values=0, axis=1)
            
            
            dta = np.abs(ta[1:, :] - ta[:-1, :])
            
            #(np.asarray(dta/1000)<limit) & 
            high_pos_x.append(np.where((temp_a1> ((time-interv)*1000)) & (temp_a1 <((time+interv)*1000)) & (ta[1:, :]> ((time-interv)*1000)) & (ta[1:, :] <((time+interv)*1000)) & (ta[:-1, :]> ((time-interv)*1000)) & (ta[:-1, :] <((time+interv)*1000)) & (ta_padded_a0[0:499, :]> ((time-interv)*1000)) & (ta_padded_a0[0:499, :] <((time+interv)*1000))  & (ta_padded_a1[0:499, 0:250]> ((time-interv)*1000)) & (ta_padded_a1[0:499, 0:250] <((time+interv)*1000)))[0])
            high_pos_y.append(np.where((temp_a1> ((time-interv)*1000)) & (temp_a1 <((time+interv)*1000)) & (ta[1:, :]> ((time-interv)*1000)) & (ta[1:, :] <((time+interv)*1000)) & (ta[:-1, :]> ((time-interv)*1000)) & (ta[:-1, :] <((time+interv)*1000)) & (ta_padded_a0[0:499, :]> ((time-interv)*1000)) & (ta_padded_a0[0:499, :] <((time+interv)*1000))  & (ta_padded_a1[0:499, 0:250]> ((time-interv)*1000)) & (ta_padded_a1[0:499, 0:250] <((time+interv)*1000)))[1])
            dta = np.abs(ta[:, 1:] - ta[:, :-1])
            high_pos_x.append(np.where((temp_a0> ((time-interv)*1000)) & (temp_a0 <((time+interv)*1000)) & (ta[:, 1:]> ((time-interv)*1000)) & (ta[:, 1:] <((time+interv)*1000)) & (ta[:, :-1]> ((time-interv)*1000)) & (ta[:, :-1] <((time+interv)*1000)) & (ta_padded_a0[0:500, 0:249]> ((time-interv)*1000)) & (ta_padded_a0[0:500,  0:249] <((time+interv)*1000))  & (ta_padded_a1[:, 0:249]> ((time-interv)*1000)) & (ta_padded_a1[:, 0:249] <((time+interv)*1000)))[0])
            high_pos_y.append(np.where((temp_a0> ((time-interv)*1000)) & (temp_a0 <((time+interv)*1000)) & (ta[:, 1:]> ((time-interv)*1000)) & (ta[:, 1:] <((time+interv)*1000)) & (ta[:, :-1]> ((time-interv)*1000)) & (ta[:, :-1] <((time+interv)*1000)) & (ta_padded_a0[0:500, 0:249]> ((time-interv)*1000)) & (ta_padded_a0[0:500,  0:249] <((time+interv)*1000))  & (ta_padded_a1[:, 0:249]> ((time-interv)*1000)) & (ta_padded_a1[:, 0:249] <((time+interv)*1000)))[1])
            
            high_pos_x = np.concatenate(tuple(high_pos_x))
            high_pos_y = np.concatenate(tuple(high_pos_y))
            d_temp = dta[high_pos_x[np.where(high_pos_y < 249)[0]], high_pos_y[np.where(high_pos_y < 249)[0]]]
            delays.append(d_temp)
            #v = mtl.read(vname)
            #interval = [0, tmax]
            #v = setup_data2(v, dims[0], dims[1], interval)
    #        if len(high_pos_x) != 0:
    #            print(time)
    #            v = mtl.read(vname)
    #            interval = [0, tmax]
    #            v = setup_data2(v, dims[0], dims[1], interval)
    #            X = np.arange(0, int(dims[0]*dx), dx)
    #            Y = np.arange(0, int(dims[1]*dx), dx)
    #            plt.figure()
    #            plt.pcolormesh(X, Y, v[:,:,int(time/dt)], cmap=plt.cm.get_cmap('hot'))
    #            plt.plot(high_pos_x*dx, high_pos_y*dx, '*', color='blue')
    #            break
            actmap.advance(10)
            time = time+10
    delays = np.concatenate(tuple(delays))/1000  
    bins, edges = np.histogram(delays, bins=np.arange(np.max(delays)), density = True)
    left,right = edges[:-1],edges[1:]
    X = np.array([left,right]).T.flatten()
    Y = np.array([bins,bins]).T.flatten()
    plt.figure()
    plt.plot(X,Y)
    plt.yscale('log')
    plt.xlim([0,int(np.max(delays))])
#    ta_test1 = ta[1:, :].ravel()
#    ta_test2 = ta[:, 1:].ravel()
    #v_singles = []
    #delay = 100
    #plt.plot(high_pos_x*dx, high_pos_y*dx, '*', color='blue')
#    for l in range(len(high_pos_x)):
#        #if ta_test1[high_pos[l]] > ((time-30)*1000) and ta_test1[high_pos[l]] < ((time)*1000) and ta_test2[high_pos[l]] > ((time-30)*1000) and ta_test2[high_pos[l]] < ((time)*1000):
#        I, J = high_pos_x[l], high_pos_y[l]
#        if J!=v.shape[0]-1 and I!=v.shape[1]-1:
#            if (ta[I,J] > ((time-delay)*1000) and ta[I,J] < ((time+delay)*1000) and ta[I+1,J] > ((time-delay)*1000) and ta[I+1,J] < ((time+delay)*1000)) and (ta[I,J+1] > ((time-delay)*1000) and ta[I,J+1] < ((time+delay)*1000)) and (ta[I,J-1] > ((time-delay)*1000) and ta[I,J-1] < ((time+delay)*1000)) and (ta[I-1,J] > ((time-delay)*1000) and ta[I-1,J] < ((time+delay)*1000)):
#                plt.plot(I*dx, J*dx, '*', color='blue')
#                vs1 = v[J,I,:]
#                vs2 = v[J-1,I,:]
#                vs3 = v[J,I-1,:]
#                vs4 = v[J+1,I,:]
#                vs5 = v[J,I+1,:]
#
#                v_singles.append([vs1,vs2,vs3,vs4,vs5, I, J, ta])
    #return v_singles

#def cycle_std_map():
if __name__ == '__main__':

    tmax = 2500
    dt = 1
    dx = 0.02
    substrate = 'S3'
    shape = (500, 250)
    plot_high_delays(substrate, 10, 2000, dx, dt, tmax)









#X = np.arange(0, tmax, dt)
#plt.figure()
#for i in range(5):
#    if len(a[i]) != 0:
#        Y = np.asarray(a[100][i])
#        plt.plot(X,Y)
#        plt.xlim([875,1000])
#cycles = cycle_calc(filename, tmax, dt)

#fig, ax = plt.subplots(3, 2, sharex='col', sharey='row')
#ax[0,0].set_title('Activation Delays')
#ax[0,1].set_title('Cycle Length')
#for k in range(len(models)):
#    if k != 1:
#        ax[k,0].set_ylabel(models[k])
#    else:
#        ax[k,0].set_ylabel('RCN')
#    for j in range(len(thresholds)):
#        filename = '/mnt/data/samuel/S1/activ_'+str(int(thresholds[j]*-1))+'_'+models[k]
#        actmap = mtl.activ_map(filename)
#        actmap.generate(225)
#        
#        info = mtl.read(filename, only_info=True)
#        dims = info['dims']
#        
#        delays = []
#        time = 225
#        interv = 30
#        for i in range(225):
#            high_pos_x = []
#            high_pos_y = []
#            ta = actmap.depol_map.reshape(dims)
#            ta_padded_a0 = np.insert(ta, 0, values=0, axis=0)
#            ta_padded_a1 = np.insert(ta, 0, values=0, axis=1)
#            temp_a0 = np.insert(ta[1:,:-1], 499, values=0, axis=0)
#            temp_a1 = np.insert(ta[:-1,1:], 249, values=0, axis=1)
#            
#            
#            dta = np.abs(ta[1:, :] - ta[:-1, :])
#            
#            #(np.asarray(dta/1000)<limit) & 
#            high_pos_x.append(np.where((temp_a1> ((time-interv)*1000)) & (temp_a1 <((time+interv)*1000)) & (ta[1:, :]> ((time-interv)*1000)) & (ta[1:, :] <((time+interv)*1000)) & (ta[:-1, :]> ((time-interv)*1000)) & (ta[:-1, :] <((time+interv)*1000)) & (ta_padded_a0[0:499, :]> ((time-interv)*1000)) & (ta_padded_a0[0:499, :] <((time+interv)*1000))  & (ta_padded_a1[0:499, 0:250]> ((time-interv)*1000)) & (ta_padded_a1[0:499, 0:250] <((time+interv)*1000)))[0])
#            high_pos_y.append(np.where((temp_a1> ((time-interv)*1000)) & (temp_a1 <((time+interv)*1000)) & (ta[1:, :]> ((time-interv)*1000)) & (ta[1:, :] <((time+interv)*1000)) & (ta[:-1, :]> ((time-interv)*1000)) & (ta[:-1, :] <((time+interv)*1000)) & (ta_padded_a0[0:499, :]> ((time-interv)*1000)) & (ta_padded_a0[0:499, :] <((time+interv)*1000))  & (ta_padded_a1[0:499, 0:250]> ((time-interv)*1000)) & (ta_padded_a1[0:499, 0:250] <((time+interv)*1000)))[1])
#            dta = np.abs(ta[:, 1:] - ta[:, :-1])
#            high_pos_x.append(np.where((temp_a0> ((time-interv)*1000)) & (temp_a0 <((time+interv)*1000)) & (ta[:, 1:]> ((time-interv)*1000)) & (ta[:, 1:] <((time+interv)*1000)) & (ta[:, :-1]> ((time-interv)*1000)) & (ta[:, :-1] <((time+interv)*1000)) & (ta_padded_a0[0:500, 0:249]> ((time-interv)*1000)) & (ta_padded_a0[0:500,  0:249] <((time+interv)*1000))  & (ta_padded_a1[:, 0:249]> ((time-interv)*1000)) & (ta_padded_a1[:, 0:249] <((time+interv)*1000)))[0])
#            high_pos_y.append(np.where((temp_a0> ((time-interv)*1000)) & (temp_a0 <((time+interv)*1000)) & (ta[:, 1:]> ((time-interv)*1000)) & (ta[:, 1:] <((time+interv)*1000)) & (ta[:, :-1]> ((time-interv)*1000)) & (ta[:, :-1] <((time+interv)*1000)) & (ta_padded_a0[0:500, 0:249]> ((time-interv)*1000)) & (ta_padded_a0[0:500,  0:249] <((time+interv)*1000))  & (ta_padded_a1[:, 0:249]> ((time-interv)*1000)) & (ta_padded_a1[:, 0:249] <((time+interv)*1000)))[1])
#            
#            high_pos_x = np.concatenate(tuple(high_pos_x))
#            high_pos_y = np.concatenate(tuple(high_pos_y))
#            d_temp = dta[high_pos_x[np.where(high_pos_y < 249)[0]], high_pos_y[np.where(high_pos_y < 249)[0]]]
#            delays.append(d_temp)
#            #v = mtl.read(vname)
#            #interval = [0, tmax]
#            #v = setup_data2(v, dims[0], dims[1], interval)
#            #X = np.arange(0, int(dims[0]*dx), dx)
#            #Y = np.arange(0, int(dims[1]*dx), dx)
#            #plt.pcolormesh(X, Y, v[:,:,int(time/dt)], cmap=plt.cm.get_cmap('hot'))
#            actmap.advance(10)
#            time = time+10
#        
#        delays = np.concatenate(tuple(delays))/1000  
#        bins, edges = np.histogram(delays, bins=np.arange(np.max(delays)), density = True)
#        left,right = edges[:-1],edges[1:]
#        X = np.array([left,right]).T.flatten()
#        Y = np.array([bins,bins]).T.flatten()
#        if j == 0:
#            l1, = ax[k, 0].plot(X,Y)
#        elif j == 1:
#            l2, = ax[k, 0].plot(X,Y)
#        elif j == 2:
#            l3, = ax[k, 0].plot(X,Y)
#        ax[k, 0].set_xlim([0,np.max(delays)])
#        ax[k, 0].set_yscale('log')
#        if k == 0 and j == 2:
#            ax[k, 0].legend((l1, l2, l3), ('-20 mV', '-40 mV', '-60 mV'), title="Activation Threshold")
#
#        delays = cycle_calc(filename, 2500, 1)
#        print([thresholds[j], models[k]])
#        #print(len(delays))
#        #print(np.max(delays))
#        bins, edges = np.histogram(delays, bins=np.arange(np.max(delays)), density = True)
#        left,right = edges[:-1],edges[1:]
#        X = np.array([left,right]).T.flatten()
#        Y = np.array([bins,bins]).T.flatten()
#        ax[k, 1].plot(X,Y)
#        ax[k, 1].set_xlim([0,int(np.max(delays))])
#        ax[k, 1].set_yscale('log')
#        
##for k in range(len(models)):
##    tmax = 2500
##    dt = 1
##    filename = '/mnt/data/samuel/S1/activ_60_' + models[k]
##    shape = (500, 250)
##    cycles = cycle_maps(filename, tmax, dt, shape, models[k])
#    
##for k in range(len(models)):
#    