#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 09:59:00 2019

@author: saliani
"""

import numpy as np
from mtlhearttools.analysis import SimulationOutput
from mtlview import mtlviewslice, mtlview, mtlview2d, encode_mpeg
import mtlheartsim as mtl

#out = SimulationOutput('/data1/saliani/spiralFA/AF1_2')
#act = out.activ_map()
#
#act = np.round(act, -1)
##view = mtlviewslice('zurich/280u', batch=True)
#view = mtlview('zurich/280u', batch=True)
#view.scalar(act)
#view.colormap('gist_earth_r')
#
#view.clim(0,150)
#
#view.run()

############




#for i in range(1,15):
#    
##viewer.rotate((1.0000,-0.7192,0.2496,-0.0041,-0.6485))
#    viewer = mtlview2d('2d', batch=True)
#    viewer('/data1/saliani/spiralFA/AF1/AF1_2/cmap')
#    viewer.goto(i)
#    viewer.save('/data1/saliani/spiralFA/AF1/AF1_2/map'+str(i)+'png')
#    viewer.run(quit=True)
#    
#    
#viewer.colormap('rainbow')
#viewer.movie(filename='frame%04d.png', tmin=0, tmax=150, resize=100, divisor=4)
#
#viewer.run(quit=True)
#
#encode_mpeg(filename='frame%04d.png', output='movie.mp4', framerate=5)


viewer = mtlview2d('/data1/saliani/spiralFA/AF5/AF5_4/cmap', batch=True)
viewer.movie(tmin=0, tmax=2500)
viewer.run(quit=True)
encode_mpeg(framerate=20)




