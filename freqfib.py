#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 12:50:47 2019

@author: saliani
"""

import numpy as np
import mtlheartsim as mtl
import matplotlib.pyplot as plt
import matplotlib as mpl
from setup import setup_data_single
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
from numpy import median
import statistics


def cycle_calc(filename, tmax, dt):
    #dims = info['dims']
    cycles = []
    actmap1 = mtl.activ_map(filename)
    actmap2 = mtl.activ_map(filename)
    actmap1.generate(0) # generate an activation map from data up to t= 0 ms
    actmap2.generate(0)
    last_map = actmap2.depol_map # depolarization map
    actmap1.advance(dt) #update map by advancing 5 ms
    current = actmap1.depol_map

    for i in range(int(tmax/dt)-1):
        cycle = (np.asarray(current[np.where((current!=-1) & (last_map!=-1))[0]] - last_map[np.where((current!=-1) & (last_map!=-1))[0]]))
        if len(np.where(cycle!= 0)[0]) != 0:
            #print(type(np.where(cycle!= 0)[0]))
            cycle = cycle[np.where(cycle!= 0)[0]]
            cycles.append(cycle)
        actmap1.advance(dt)
        actmap2.advance(dt)
        last_map = actmap2.depol_map
        current = actmap1.depol_map

    cycles = np.concatenate(tuple(cycles))/1000
    return cycles

def cycle_maps(filename, start, tmax, dt, shape, model):
    actmap1 = mtl.activ_map(filename)
    actmap2 = mtl.activ_map(filename)
    actmap1.generate(start)
    actmap2.generate(start)
    last_map = actmap2.depol_map
    actmap1.advance(dt)
    current = actmap1.depol_map
    values = [ [] for i in range(len(current))]
    
    
    for i in range(int((tmax-start)/dt)-1):
        cycle = (np.asarray(current - last_map))
        indexes = np.where((cycle != 0) & (last_map!=-1))[0]
        if len(indexes) != 0:
#            print(indexes)
#            values[indexes].append(cycle[indexes])
            for j in range(len(indexes)):
                values[indexes[j]].append(cycle[indexes[j]])
#                print(len(cycle[indexes]))
#                print(j)
                
        actmap1.advance(dt)
        actmap2.advance(dt)
        
    stds = np.zeros(len(current))
    means = np.zeros(len(current))
    medians = np.zeros(len(current))
    
    
    for i in range(len(current)):
        stds[i] = np.std(np.asarray(values[i])/1000)
        means[i] = np.mean(np.asarray(values[i])/1000)
        medians[i] = np.median(np.asarray(values[i])/1000)
        
        mean_full = np.mean(means[i])
        stds_full = np.mean(stds[i])
        med_median = np.median([medians[i]])
        
    print(mean_full)
    print(stds_full)
    print(med_median)
        
        
        
    std_array = setup_data_single(stds, shape[0], shape[1])
    mean_array = setup_data_single(means, shape[0], shape[1])
    
    fig, ax = plt.subplots(2, 1, sharex='col', sharey='row')
    X = np.arange(0, int(shape[0]*0.02), 0.02)
    Y = np.arange(0, int(shape[1]*0.02), 0.02)
    Im1 = ax[0].pcolormesh(X, Y,mean_array, cmap=plt.cm.get_cmap('hot'), vmin = 140 , vmax = 200)

    divider = make_axes_locatable(ax[0])
    cax = divider.append_axes('right', size='5%', pad=0.05)
#    fig.colorbar(Im1, cax=cax, orientation='vertical', boundaries=np.linspace(140, 220, 5))
    fig.colorbar(Im1, cax=cax, orientation='vertical')

    ax[0].axis([0, int(shape[0]*0.02), 0, int(shape[1]*0.02)])
    ax[0].set_aspect('equal')
    ax[0].set_ylabel('Mean')
    ax[0].set_title('Cell cycle length distributions ' + model)

    Im2 = ax[1].pcolormesh(X, Y,std_array, cmap=plt.cm.get_cmap('hot'), vmin = 0, vmax = 60)

    divider2 = make_axes_locatable(ax[1])
    cax2 = divider2.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(Im2, cax=cax2, orientation='vertical')
    ax[1].axis([0, int(shape[0]*0.02), 0, int(shape[1]*0.02)])
    ax[1].set_aspect('equal')
    ax[1].set_ylabel('Standard Deviation')


if __name__ == '__main__':

    tmax = 2500
    start = 500
    dt = 1
    dx = 0.02
    shape = (500, 250)
    model='CRN'
    filename = '/data1/saliani/spiralFA/AF5_40mV/AF5_8/activ'
#    a = cycle_calc(filename, tmax, dt)
    cycle_maps(filename, start, tmax, dt, shape, model)
    
