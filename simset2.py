#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 13:13:02 2019

@author: samuel
"""

import mtlheartsim as mtl
import numpy as np
#g = [0.05,0.055,0.06,0.065]
g = [0.07,0.075]
aniso = [(1,0.5), (2,0.5), (3,0.5), (4,0.5)]


shape = 500, 250

left_half = np.zeros(shape, dtype=int)
left_half[:250, :] = 1

bottom_half = np.zeros(shape, dtype=int)
bottom_half[:, :125] = 1
th = -40

##for i in range(len(g)):
##    for j in range(len(aniso)):
##        print(['G_si', 'Ratio'])
##        print([g[i], aniso[j]])
##        sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
##        sim.property({'G_si': g[i], 'G_K': 0.423})
##    
##        sim.initial_condition(filename = '/home/saliani/repos/spiralFA/dumpfinal_CRN1')
##
##        sim.coupling(sigma=aniso[j])
##        sim.output_dir('/mnt/data/samuel/S2')
##        sim.time_integration(2500)
##        sim.output_maps(filename = ('cmap_'+str(g[i])+'_'+str(int(aniso[j][0]/aniso[j][1]))), spacing=5)
##        sim.output_activ(filename = ('activ_'+str(g[i])+'_'+str(int(aniso[j][0]/aniso[j][1]))), thres = th, tablespacing = 1)
##        sim.output_var(filename = ('vmmap_'+str(g[i])+'_'+str(int(aniso[j][0]/aniso[j][1]))), spacing = 0.1)
##        sim.run()
#
################################# AF1      
#for i in range(len(aniso)):
#    
#    sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
#    sim.property({'PCaL': 0.3, 'Pto': 0.3, 'PK1': 1.30}) #AF1
#
#    sim.initial_condition(filename = '/home/saliani/repos/spiralFA/dumpfinal_CRN')
#
#    sim.coupling(sigma=aniso[i])
#    sim.output_dir('/home/saliani/repos/spiralFA/AF1')
#    sim.time_integration(2500)
#    sim.output_maps(filename = ('cmap_AF1'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing=5)
#    sim.output_activ(filename = ('activ_AF1'+'_'+str(int(aniso[i][0]/aniso[i][1]))), thres = th, tablespacing = 1)
#    sim.output_var(filename = ('vmmap_AF1'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing = 0.1)
#    sim.run()
#        
#################################AF2        
#for i in range(len(aniso)):
#    
#    sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
#    sim.property({'PCaL': 0.37, 'Pto': 0.35, 'PK1': 1.73}) #AF2
#
#    sim.initial_condition(filename = '/home/saliani/repos/spiralFA/dumpfinal_CRN')
#
#    sim.coupling(sigma=aniso[i])
#    sim.output_dir('/home/saliani/repos/spiralFA/AF2')
#    sim.time_integration(2500)
#    sim.output_maps(filename = ('cmap_AF2'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing=5)
#    sim.output_activ(filename = ('activ_AF2'+'_'+str(int(aniso[i][0]/aniso[i][1]))), thres = th, tablespacing = 1)
#    sim.output_var(filename = ('vmmap_AF2'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing = 0.1)
#    sim.run() 
#        
##################################AF3        
#for i in range(len(aniso)):
#    
#    sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
#    sim.property({'PCaL': 0.37, 'PKur': 0.51, 'Pto': 0.34, 'PK1': 2.06}) #AF2
#
#    sim.initial_condition(filename = '/home/saliani/repos/spiralFA/dumpfinal_CRN')
#
#    sim.coupling(sigma=aniso[i])
#    sim.output_dir('/home/saliani/repos/spiralFA/AF3')
#    sim.time_integration(2500)
#    sim.output_maps(filename = ('cmap_AF3'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing=5)
#    sim.output_activ(filename = ('activ_AF3'+'_'+str(int(aniso[i][0]/aniso[i][1]))), thres = th, tablespacing = 1)
#    sim.output_var(filename = ('vmmap_AF3'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing = 0.1)
#    sim.run()  
      
#################################AF4        
# for i in range(len(aniso)):

#    sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
#    sim.property({'PCaL': 0.37, 'PKur': 0.51, 'Pto': 0.34, 'PK1': 2.06, 'PKs': 2}) #AF2

#    sim.initial_condition(filename = '/home/saliani/repos/spiralfareentry/dumpfinal_CRN')

#    sim.coupling(sigma=aniso[i])
#    sim.output_dir('/home/saliani/repos/spiralfareentry/AF4bis')
#    sim.time_integration(2500)
#    sim.output_maps(filename = ('cmap_AF4'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing=5)
#    sim.output_activ(filename = ('activ_AF4'+'_'+str(int(aniso[i][0]/aniso[i][1]))), thres = th, tablespacing = 1)
#    sim.output_var(filename = ('vmmap_AF4'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing = 0.1)
#    sim.run()
    
#################################AF5        
for i in range(len(aniso)):
    
    sim = mtl.Simulation('CRN', '2d', shape=shape, dx=0.02)
    sim.property({'PCaL': 0.35, 'PKur': 0.50, 'Pto': 0.50, 'PK1': 1.75, 'PKs': 2, 'PKr': 5}) #AF2

    sim.initial_condition(filename = '/data1/saliani/spiralFA/dumpfinal_CRN')

    sim.coupling(sigma=aniso[i])
    sim.output_dir('/data1/saliani/spiralFA/AF5')
    sim.time_integration(2500)
    sim.output_maps(filename = ('cmap_AF5'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing=5)
    sim.output_activ(filename = ('activ_AF5'+'_'+str(int(aniso[i][0]/aniso[i][1]))), thres = th, tablespacing = 1)
    sim.output_var(filename = ('vmmap_AF5'+'_'+str(int(aniso[i][0]/aniso[i][1]))), spacing = 0.1)
    sim.run()
    
###################################### output VM, ta, APD and w
#
#import mtlheartsim as mtl
#import numpy as np
#
## x = action potential voltage
## idx = list of node number
## ta = list of activation times
## depo = depolarization time
## w = spiral wave frequency = 1/APD
#
#x = mtl.read('vmmap')
#idx, ta, depol = mtl.read('activ')
#max=np.amax(ta)/1000
#min=np.amin(ta)/1000
#activtime=max-min
#print(activtime)

