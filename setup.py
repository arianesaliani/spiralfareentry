#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 13:35:54 2018

@author: samuel
Nx: Size in y axis of space
Ny: Size in y axis of space
dx: x resolution 
dy: y resolution 
v: Potential vector matrix for everytime step
"""
import numpy as np
import math

def setup_data(v, Nx, Ny):
    potentials = np.zeros((Ny,Nx,len(v[1])-1))
    
    temp_range = range(Nx*Ny)
    space_range = [x+1 for x in temp_range]
    for j in range(len(v[1])-1):
        for i in (space_range):
            y_pos = np.remainder(i,Ny)-1
            if y_pos < 0:
                y_pos = Ny-1
                #print(x_pos)
                #print(y_pos)
            if i == 0:
                y_pos = 0
            #print(i)
            #print(x_pos)
            x_pos = math.ceil(i/Ny)-1
            t_pos = j
            potentials[y_pos,x_pos,t_pos] = v[i,j]
    return potentials

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 13:35:54 2018

@author: samuel
Nx: Size in y axis of space
Ny: Size in y axis of space
dx: x resolution 
dy: y resolution 
v: Potential vector matrix for everytime step
"""

def setup_data2(v, Nx, Ny, interval):
    potentials = np.zeros((Ny,Nx,interval[1]-interval[0]))
    
    for j in range((interval[1]-interval[0])):
        for i in range(Nx):
            #y_pos = np.remainder(i,Ny)-1
            #if y_pos < 0:
                #y_pos = Ny-1
                #print(x_pos)
                #print(y_pos)
            #if i == 0:
                #y_pos = 0
            #print(i)
            #print(x_pos)
            #x_pos = math.ceil(i/Ny)-1
            t_pos = j
            #print(potentials[0:((Ny)-1),i,t_pos])
            #print(v[((i*Ny)+1):((i+1)*Ny),j])
            potentials[0:((Ny)),i,t_pos] = v[((i*Ny)):(((i+1)*Ny)),j+interval[0]]
            #print(potentials[0:((Ny)-1),i,t_pos])
    return potentials

def setup_data_single(v, Nx, Ny):
    potentials = np.zeros((Ny,Nx))
    for i in range(Nx):
        potentials[0:((Ny)),i] = v[((i*Ny)):(((i+1)*Ny))]
    return potentials