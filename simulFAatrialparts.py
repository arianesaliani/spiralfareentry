#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 14:56:30 2019

@author: saliani
"""

import mtlheartsim as mtl
import numpy as np


aniso = [(2,0.5)]
th = -70

sim = mtl.Simulation('CRN', 'cubic', geom='/home/jacquem/heart/mesh/zurich/280u')

#sim.initial_condition(filename = '/home/saliani/repos/spiralfareentry/dumpfinal_CRN_3D')

sim.coupling(sigma=aniso[0])
#sim.time_integration(2500)
sim.time_integration(150, dt=0.1, splitting=[2, 8, 3])
sim.stimulus(time=0, strength=30, nodes='sinus.nodes')

sim.property({'PCaL': 0.3, 'Pto': 0.15, 'PK1': 1.3}, nodes = ('atria.part', 'CT')) #AF1 in PM
#sim.property({'PCaL': 0.35, 'Pto': 0.5, 'PK1': 1.75, 'PKur' : 0.5, 'PKs' : 2, 'PKr' : 5}, nodes = ('atria.part', 'PM')) #AF1 in PM

#sim.coupling((9, 3))

sim.output_dir('/data1/saliani/FAatrialparts/CT_ferrer/AF1')
sim.output_maps(spacing=5)
sim.output_var(spacing=0.1)
sim.output_activ(thres = th, tablespacing = 1)
sim.output_iter(1)

sim.run()

#import numpy as np
#from mtlhearttools.analysis import SimulationOutput
#from mtlview import mtlviewslice, mtlview, encode_mpeg
#import mtlheartsim as mtl
#
#out = SimulationOutput('/data1/saliani/FAatrialparts/PM/AF1')
#act = out.activ_map()
#
#act = np.round(act, -1)
#view = mtlviewslice('/home/jacquem/heart/mesh/zurich/280u', batch=True)
#view = mtlview('/home/jacquem/heart/mesh/zurich/280u', batch=True)
#view.scalar(act)
#view.colormap('gist_earth_r')
#
#view.clim(0,150)
#
#view.run()


##########
#
#viewer = mtlview('/home/jacquem/heart/mesh/zurich/280u', batch=True)
#viewer('/data1/saliani/FAatrialparts/PM/AF1/cmap')
#viewer.movie(tmin=0, tmax=150)
#viewer.run(quit=True)
#encode_mpeg(framerate=20)
#
##########
#
#
#for i in range(1,150):
#    viewer = mtlview('/home/jacquem/heart/mesh/zurich/280u', batch=True)
#    viewer('/data1/saliani/FAatrialparts/PM/AF1/cmap')
#    viewer.rotate((1.0000,-0.7192,0.2496,-0.0041,-0.6485))
#    viewer.goto(i)
#    viewer.save('/home/saliani/FAatrialparts/PM/AF1/map'+str(i)+'png')
#    
#    viewer.run()
#    
#
#
###########
#
#
##for i in range(1,150):
#    
#viewer = mtlview('zurich/280u', batch=True)
#viewer('cmap')
##viewer.rotate((1.0000,-0.7192,0.2496,-0.0041,-0.6485))
##    viewer.goto(i)
##    viewer.save('/home/saliani/map'+str(i)+'png')
##    viewer.run(quit=True)
#viewer.colormap('rainbow')
#viewer.movie(filename='frame%04d.png', tmin=0, tmax=150, crop=False, divisor=4)
#
#viewer.run(quit=True)
#
#encode_mpeg(filename='frame%04d.png', output='movie.mp4', framerate=5)


#               # assuming 'rightleft.part' defines two regions 'LA' and 'RA'
#               sim.coupling_partition({'LA': 3, 'RA': 4},
 #                                     partition='rightleft.part')
#



